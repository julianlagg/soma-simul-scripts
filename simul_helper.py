import h5py as h5
from numpy import *
import numpy as np # ugh
import sys
import os
import re
from enum import Enum
#import matplotlib.pyplot as plt
import time
import random
import subprocess



class ArchType(Enum):
    DIBLOCK = 1
    TRIBLOCK = 2
    QUADBLOCK = 3

    # dont want class name as part of the enum
    def __str__(self):
        return self.name

nref = 32 # length of the diblock
poly_length_in_nref = {
        ArchType.DIBLOCK : 1,
        ArchType.TRIBLOCK : 2,
        ArchType.QUADBLOCK : 4
        }
curly_smiles_arch = {
        ArchType.DIBLOCK : "A{16}B{16}",
        ArchType.TRIBLOCK : "A{16}B{32}A{16}",
        ArchType.QUADBLOCK : "A{16}B{32}A{32}B{32}A{16}"
        }
n_beads_per_polymer = {
        ArchType.DIBLOCK : 32,
        ArchType.TRIBLOCK : 64,
        ArchType.QUADBLOCK : 128
        }


class XmlInputParams():

    def __init__(self, arch, nx, analysis = "", cubic=False):

        assert(isinstance(nx, int))
        assert(isinstance(arch, ArchType))

        n_beads_per_poly = n_beads_per_polymer[arch]
        # make the box a cube
        self.nx = nx
        if not cubic:
            self.ny = 16
            self.nz = 16
        else:
            self.ny = nx
            self.nz = nx
        self.lx = (self.nx/24)*1.677
        self.ly = (self.ny/24)*1.677
        self.lz = (self.nz/24)*1.677
        self.poly_arch = curly_smiles_arch[arch]
        self.analysis = analysis
        
        # want to keep this constant for across simulations
        # this expression is n_poly*n_beads_per_poly/(nx*ny*nz)
        # with values from the diblock.xml file in SOMAs testing dir
        n_beads_per_cell = 120*64/(12*6*6)

        n_cells = self.nx*self.ny*self.nz
        self.n_poly = round(n_beads_per_cell * n_cells / (n_beads_per_poly))
    
    def replace(self, s):

        return str(self.__dict__[s.group(1)])


def add_external_field(conf_f, A, num_waves=1, show=False):
    
    with h5.File(conf_f, "a") as f:
        e = f["external_field"]
        p = f["parameter"]
        
        nx,ny,nz = p["nxyz"]
        xv = linspace(0,num_waves*2*pi,nx)
        APart = A/2
        x = array(e)
        x_type0 = x[0]
        x_type1 = x[1]
        for iy in range(ny):
            for iz in range(nz):
                x_type0[:,iy,iz] = sin(xv) * APart
                x_type1[:,iy,iz] = -sin(xv) * APart
        e[0,:,:,:] = x_type0
        e[1,:,:,:] = x_type1
    
    if show:

        with h5.File(conf_f, "r") as f:
            e = np.array(f["external_field"])

        plt.plot(e[0,:,0,0])
        plt.title("showing external field")
        plt.show()

    

def remove_external_field(conf_f):

    with h5.File(conf_f, "a") as f:
        del f["external_field"]


simul_root_dir = None
soma_install_root_dir = None
xml_file_rex = r"([^\.\s]+).xml$"
h5_file_rex = r"([^\.\s]+).h5$"

def init_simul_root_dir(dirname, create=True):
    global simul_root_dir
    if simul_root_dir is not None:
        os.chdir(simul_root_dir)
        os.chdir("..")
    if create:
        if os.mkdir(dirname):
            raise ValueError("failed to create directory")
    simul_root_dir = os.path.abspath(dirname)

def init_soma_install_root_dir(dirname):
    global soma_install_root_dir
    if soma_install_root_dir is not None:
        raise ValueError("can only initialize soma dir once")
    soma_install_root_dir = os.path.abspath(dirname)
    print("soma is expected to be at "+soma_install_root_dir)

def get_dirname(poly_arch, nx):
    return f"{str(poly_arch)}_nx{nx}"

def goto_simul(poly_arch, nx):
    global simul_root_dir 
    if simul_root_dir is not None:
        os.chdir(os.path.join(simul_root_dir , get_dirname(poly_arch, nx)))
    else:
        raise ValueError("must call init_simul_root_dir first")

def init_simul(poly_arch, nx):

    assert(simul_root_dir is not None)
    ret = os.mkdir(os.path.join(simul_root_dir, get_dirname(poly_arch, nx)))
    assert(not ret)

def wait_for_free_gpu():
    
    c = 0
    while(True): # never give up -> bad?
        gpu_list = subprocess.check_output("nvidia-smi --query-gpu=index,utilization.gpu --format=csv,noheader,nounits", shell=True)
        for gpu in gpu_list.splitlines():
            ls = gpu.split(b", ")
            ind = int(ls[0]); consumption = int(ls[1])
            if (consumption < 15):
                return ind
        c += 1
        print(f"failed to get free gpu, retrying in 10 seconds, (so far, tried c={c} times.)")
        time.sleep(10)

    
def call_soma(t, anafile="ana.h5", conffile="conf.h5", ompthreads=8, endfilename="end_config.h5", nrandomq=100, single_gpu=False, any_gpu=False):

    assert(re.match(h5_file_rex,conffile))
    assert(re.match(h5_file_rex,anafile))
    assert(re.match(h5_file_rex,endfilename))

    par_arg = f"--only-gpu={wait_for_free_gpu()}" if single_gpu else f"-n {ompthreads}"
    if any_gpu:
        par_arg = ""

    ret = os.system(f"{soma_install_root_dir}/bin/SOMA -t {t} --ana-file={anafile} --coord-file={conffile} {par_arg} --rng-seed={random.randrange(2**31)} --final-file={endfilename} --n_random_q={nrandomq}")
    if ret != 0:
        raise ValueError("soma had some error")

def call_conf_gen(xml_filename):

    assert(re.match(xml_file_rex, xml_filename))

    ret = os.system(f"python {soma_install_root_dir}/python-script/ConfGen.py -o conf.h5 -i {xml_filename} --ana-filename ana.h5")

    if ret != 0:
        raise ValueError("ConfGen had some error")


def call_ana_gen(xml_filename, conf_file, ana_file_out):

    assert(re.match(xml_file_rex, xml_filename))
    assert(re.match(h5_file_rex,conf_file))
    assert(re.match(h5_file_rex,ana_file_out))

    ret = os.system(f"python {soma_install_root_dir}/python-script/AnaGen.py -i {xml_filename} -c {conf_file} --ana-filename {ana_file_out}")
    if ret != 0:
        raise ValueError("AnaGen had some error")

def make_xml_input(filename, poly_arch, nx, with_ana, cubic=False):


    # todo: change to enum or something
    if with_ana==None:
        ana_tmp = very_slim_ana_template
    elif with_ana == True:
        ana_tmp = long_ana_template
    else:
        ana_tmp = slim_ana_template

    params = XmlInputParams(poly_arch, nx, ana_tmp, cubic)
    xml_out = re.sub(r"@(\w*)@", params.replace, xml_template)
    with open(filename, "x") as f:
        f.write(xml_out)

def create_xdmf(h5file, is_conf=False):

    assert(re.match(h5_file_rex,h5file))
    out_file_name = h5file.split(".")[0] + ".xmf"

    if is_conf:
        prefix = "-c"
    else:
        prefix = "-a"

    ret = os.system(f"python {soma_install_root_dir}/python-script/create_xdmf.py {prefix} {h5file} -o {out_file_name}")
    if ret != 0:
        raise ValueError("create xdmf had some error")

def get_normed_phi(ana_file):

    assert(re.match(h5_file_rex, ana_file))

    with h5.File(ana_file, "r") as f:
        phi = np.array(f["density_field"], dtype="float64")
        att = f["density_field"].attrs
        typescale = att["typescale"]
        ntypes = att["ntypes"][0]

    assert(ntypes == len(phi[0,:,0,0,0]))
    
    phi_normed = np.full_like(phi, np.NaN)
    for i in range(ntypes):
        phi_normed[:,i,:,:,:] = phi[:,i,:,:,:]*typescale[i]

    return phi_normed


def calc_profiles(ana_file):

    phi = get_normed_phi(ana_file)

    phiDiff = phi[:,0,:,:,:] - phi[:,1,:,:,:]

    phiX = phiDiff.mean(axis=(2,3))

    return phiX

def extract_profile(ana_file):

    prof = calc_profiles(ana_file)

    with h5.File("profile.h5", "w") as f:
        f.create_dataset("profile", data=prof)

def cleanup_simul_dir():

    os.remove("equilibriated.h5")
    os.remove("after_decay.h5")
    os.remove("ana.h5")
    os.remove("decay_ana.h5")

def plot_profiles(ana_file):

    phiTX = calc_profiles(ana_file)
    
    plt.imshow(phiTX)
    plt.colorbar()
    plt.show()



very_slim_ana_template="""
<density_field>
<DeltaMC> 1000</DeltaMC>
</density_field>
"""
    

slim_ana_template = """
    <density_field>
    <DeltaMC> 50 </DeltaMC>
    </density_field>
    """
    
long_ana_template = """
    <Re>
      <DeltaMC> 200 </DeltaMC>
    </Re>
    <Rg>
      <DeltaMC> 200 </DeltaMC>
    </Rg>
    <bond_anisotropy>
      <DeltaMC> 200 </DeltaMC>
    </bond_anisotropy>
    <acc_ratio>
      <DeltaMC> 200 </DeltaMC>
    </acc_ratio>
    <MSD>
      <DeltaMC> 200 </DeltaMC>
    </MSD>
    <dump>
      <DeltaMC> 50000 </DeltaMC>
    </dump>
    <density_field>
      <DeltaMC> 50 </DeltaMC>
    </density_field>
    <non_bonded_energy>
      <DeltaMC>1000</DeltaMC>
    </non_bonded_energy>
    <bonded_energy>
      <DeltaMC>1000</DeltaMC>
    </bonded_energy>
    <static_structure_factor>
      <DeltaMC> 200 </DeltaMC>
<q>5.994690811858878 5.620022636117698 5.289433069287245 4.995575676549065 4.732650640941219 4.496018108894158 4.2819220084706275 4.08728918990378 3.90958096425579 3.746681757411798 3.5968144871153265 3.4584754683801218 3.3303837843660427 3.21144150635297 3.1007021440649365 2.997345405929439 2.9006568444478438 2.810011318058849 2.7248594599358538 2.6447165346436226 2.569153205082376 2.4977878382745327 2.4302800588617073 2.3663253204706094 2.305650312253414 2.248009054447079 2.1931795653142236 2.1409610042353138 2.0911712134391434 2.04364459495189 1.9982302706196258 1.954790482127895 1.9131991952741099 1.873340878705899 1.8351094322016972</q>
    </static_structure_factor>
    <dynamical_structure_factor>
      <DeltaMC> 50 </DeltaMC>
<q>5.994690811858878 5.620022636117698 5.289433069287245 4.995575676549065 4.732650640941219 4.496018108894158 4.2819220084706275 4.08728918990378 3.90958096425579 3.746681757411798 3.5968144871153265 3.4584754683801218 3.3303837843660427 3.21144150635297 3.1007021440649365 2.997345405929439 2.9006568444478438 2.810011318058849 2.7248594599358538 2.6447165346436226 2.569153205082376 2.4977878382745327 2.4302800588617073 2.3663253204706094 2.305650312253414 2.248009054447079 2.1931795653142236 2.1409610042353138 2.0911712134391434 2.04364459495189 1.9982302706196258 1.954790482127895 1.9131991952741099 1.873340878705899 1.8351094322016972</q>
    </dynamical_structure_factor>
"""

xml_template = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<soma version="0.2.0">
  <interactions>
    <kappaN>
      50
    </kappaN>
    <chiN>
      A B 0
    </chiN>
    <bonds>
    A A  harmonic
    A B  harmonic

    B B  harmonic
    </bonds>
  </interactions>
  <A>
    <dt>
    0.17
    </dt>
  </A>
  <time>
    0
  </time>
    <reference_Nbeads>
    32
  </reference_Nbeads>
  <lxyz>
    @lx@ @ly@ @lz@ 
  </lxyz>
  <nxyz>
    @nx@ @ny@ @nz@
  </nxyz>
  <poly_arch>
     @n_poly@ @poly_arch@
  </poly_arch>
  <external_field>
      <point_cloud>
          0 0 0 0 1
      </point_cloud>
  </external_field>
  <analysis>
      @analysis@
  </analysis>
</soma>
"""

