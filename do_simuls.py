from simul_helper import *
import random

import sys

prefix = sys.argv[1]
print("doing simuls with prefix ", prefix)

random.seed()

init_soma_install_root_dir("/home/jlagg/SOMA_MASTER/cuda05_niklas")

import os
os.chdir("/home/jlagg/simuls/")

for i in range(0,50):

    init_simul_root_dir(prefix+"_"+str(i), create=True)

    for arch in [ArchType.DIBLOCK, ArchType.TRIBLOCK, ArchType.QUADBLOCK]:

        for nx in range(15,50):

            init_simul(arch, nx)
            goto_simul(arch, nx)
            equibxml = "equilibriation.xml"
            make_xml_input(equibxml, arch, nx, with_ana=None)
            call_conf_gen(equibxml)
            add_external_field("conf.h5", 3)
            call_soma(t=1000, endfilename="equilibriated.h5", single_gpu=True)
            decayxml = "decay.xml"
            make_xml_input(decayxml, arch, nx, with_ana=False)
            call_ana_gen(decayxml, "equilibriated.h5", "decay_ana.h5")
            remove_external_field("equilibriated.h5")
            call_soma(t=2000, anafile="decay_ana.h5", conffile="equilibriated.h5", endfilename="after_decay.h5", single_gpu=True)
            extract_profile("decay_ana.h5")
            cleanup_simul_dir()
            #create_xdmf("decay_ana.h5")
            #create_xdmf("ana.h5")
            #create_xdmf("conf.h5", is_conf=True)
            #create_xdmf("equilibriated.h5", is_conf=True)



