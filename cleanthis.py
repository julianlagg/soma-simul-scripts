from glob import glob
from simul_helper import *
import os

simuls = glob("*BLOCK_nx*")

for s in simuls:

	os.chdir(s)
	if os.path.isfile("decay_ana.h5"):
		extract_profile("decay_ana.h5")
		cleanup_simul_dir()   
	os.chdir("..")
